/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicaexamen2;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author sebas
 */
public class JintFacturas extends javax.swing.JDialog {

    /**
     * Creates new form JintFacturas
     */
    public JintFacturas(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setSize(950,890);
        this.deshabilitar();
    }
    
    public void deshabilitar(){
        this.txtNumNota.setEnabled(false);
        this.txtFecha.setEnabled(false);
        this.txtConcepto.setEnabled(false);
        this.txtCantidad.setEnabled(false);
        this.txtPrecio.setEnabled(false);
        this.cmbTipoPago.setEnabled(false);
        this.txtCalcularPago.setEnabled(false);
        this.txtCalcularIva.setEnabled(false);
        
        this.btmGuardar.setEnabled(false);
        this.btmMostrar.setEnabled(false);
    }
    
    public void habilitar(){
        this.txtNumNota.setEnabled(!false);
        this.txtFecha.setEnabled(!false);
        this.txtConcepto.setEnabled(!false);
        this.txtCantidad.setEnabled(!false);
        this.txtPrecio.setEnabled(!false);
        this.cmbTipoPago.setEnabled(!false);
        
        this.btmGuardar.setEnabled(true);
        this.btmMostrar.setEnabled(true);
    }
    
    public void limpiar(){
        this.txtNumNota.setText("");
        this.txtFecha.setText("");
        this.txtConcepto.setText("");
        this.txtCantidad.setText("");
        this.txtPrecio.setText("");
        this.cmbTipoPago.setSelectedIndex(0);
        this.txtCalcularPago.setText("");
        this.txtCalcularIva.setText("");
    }
    
    
    
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btmNuevo = new javax.swing.JButton();
        btmGuardar = new javax.swing.JButton();
        btmMostrar = new javax.swing.JButton();
        btmLimpiar = new javax.swing.JButton();
        btmCerrar = new javax.swing.JButton();
        btmCancelar = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtNumNota = new javax.swing.JTextField();
        txtFecha = new javax.swing.JTextField();
        txtConcepto = new javax.swing.JTextField();
        txtCantidad = new javax.swing.JTextField();
        txtPrecio = new javax.swing.JTextField();
        cmbTipoPago = new javax.swing.JComboBox<>();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtCalcularPago = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtCalcularIva = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(null);

        btmNuevo.setText("Nuevo");
        btmNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btmNuevoActionPerformed(evt);
            }
        });
        getContentPane().add(btmNuevo);
        btmNuevo.setBounds(830, 30, 67, 25);

        btmGuardar.setText("Guardar");
        btmGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btmGuardarActionPerformed(evt);
            }
        });
        getContentPane().add(btmGuardar);
        btmGuardar.setBounds(830, 80, 79, 25);

        btmMostrar.setText("Mostrar");
        btmMostrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btmMostrarActionPerformed(evt);
            }
        });
        getContentPane().add(btmMostrar);
        btmMostrar.setBounds(830, 130, 77, 25);

        btmLimpiar.setText("Limpiar");
        btmLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btmLimpiarActionPerformed(evt);
            }
        });
        getContentPane().add(btmLimpiar);
        btmLimpiar.setBounds(650, 390, 75, 25);

        btmCerrar.setText("Cerrar");
        btmCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btmCerrarActionPerformed(evt);
            }
        });
        getContentPane().add(btmCerrar);
        btmCerrar.setBounds(740, 390, 69, 25);

        btmCancelar.setText("Cancelar");
        btmCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btmCancelarActionPerformed(evt);
            }
        });
        getContentPane().add(btmCancelar);
        btmCancelar.setBounds(830, 390, 90, 25);

        jLabel2.setText("Numero de Nota:");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(80, 40, 100, 16);

        jLabel3.setText("Fecha:");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(80, 80, 39, 16);

        jLabel4.setText("Concepto:");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(80, 120, 60, 16);

        jLabel5.setText("Cantidad:");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(80, 160, 60, 16);

        jLabel6.setText("Precio:");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(80, 200, 40, 16);

        jLabel7.setText("Tipo de Pago:");
        getContentPane().add(jLabel7);
        jLabel7.setBounds(80, 240, 80, 16);
        getContentPane().add(txtNumNota);
        txtNumNota.setBounds(180, 40, 69, 22);
        getContentPane().add(txtFecha);
        txtFecha.setBounds(120, 80, 110, 22);
        getContentPane().add(txtConcepto);
        txtConcepto.setBounds(140, 120, 150, 22);
        getContentPane().add(txtCantidad);
        txtCantidad.setBounds(140, 160, 100, 22);
        getContentPane().add(txtPrecio);
        txtPrecio.setBounds(120, 200, 110, 22);

        cmbTipoPago.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Contado", "Credito" }));
        getContentPane().add(cmbTipoPago);
        cmbTipoPago.setBounds(170, 240, 90, 22);

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.setLayout(null);

        jLabel1.setText("Calcular Pago");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(30, 30, 78, 16);
        jPanel1.add(txtCalcularPago);
        txtCalcularPago.setBounds(110, 30, 120, 22);

        jLabel8.setText("Calcular IVA");
        jPanel1.add(jLabel8);
        jLabel8.setBounds(30, 90, 70, 16);
        jPanel1.add(txtCalcularIva);
        txtCalcularIva.setBounds(110, 90, 90, 22);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(70, 300, 470, 160);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btmNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btmNuevoActionPerformed
        // TODO add your handling code here:
        factura = new Facturas();
        this.habilitar();
    }//GEN-LAST:event_btmNuevoActionPerformed

    private void btmLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btmLimpiarActionPerformed
        // TODO add your handling code here:
        this.txtNumNota.setText("");
        this.txtFecha.setText("");
        this.txtConcepto.setText("");
        this.txtCantidad.setText("");
        this.txtPrecio.setText("");
        this.cmbTipoPago.setSelectedIndex(0);
        this.txtCalcularPago.setText("");
        this.txtCalcularIva.setText("");
    }//GEN-LAST:event_btmLimpiarActionPerformed

    private void btmCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btmCerrarActionPerformed
        // TODO add your handling code here:
        int opcion = 0;
        opcion = JOptionPane.showConfirmDialog(this, "Deseas salir",
            "Facturas", JOptionPane.YES_NO_OPTION);
        if (opcion == JOptionPane.YES_OPTION){
            this.dispose();

        }
        
    }//GEN-LAST:event_btmCerrarActionPerformed

    private void btmCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btmCancelarActionPerformed
        // TODO add your handling code here:
        this.limpiar();
        this.deshabilitar();
        
    }//GEN-LAST:event_btmCancelarActionPerformed

    private void btmGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btmGuardarActionPerformed
        // TODO add your handling code here:
      boolean exito = true; 

    if (txtNumNota.getText().equals("")) exito = false;
    if (txtFecha.getText().equals("")) exito = false;
    if (txtConcepto.getText().equals("")) exito = false;
    if (txtCantidad.getText().equals("")) exito = false;
    if (txtPrecio.getText().equals("")) exito = false;
    if (cmbTipoPago.getSelectedIndex() == -1) exito = false;

    if (exito) {
            factura = new Facturas();

            factura.setNumNota(Integer.parseInt(txtNumNota.getText())); 
            factura.setFecha(txtFecha.getText());
            factura.setConcepto(txtConcepto.getText());
            factura.setCantidad(Integer.parseInt(txtCantidad.getText()));
            factura.setTipoPago(cmbTipoPago.getSelectedIndex() + 1); 

            Producto producto = new Perecederos(); 
            producto.setPrecioUnitario(Integer.parseInt(txtPrecio.getText())); 
            factura.setProducto(producto);

            JOptionPane.showMessageDialog(this, "Se guardó con éxito");
            btmMostrar.setEnabled(true);
       
    } else {
        JOptionPane.showMessageDialog(this, "Por favor, llene todos los campos.");
    }

    }//GEN-LAST:event_btmGuardarActionPerformed

    private void btmMostrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btmMostrarActionPerformed
        // TODO add your handling code here:
       if (factura != null) {
            this.txtNumNota.setText(String.valueOf(factura.getNumNota()));
            this.txtFecha.setText(factura.getFecha());
            this.txtConcepto.setText(factura.getConcepto());
            this.txtCantidad.setText(String.valueOf(factura.getCantidad()));
            this.cmbTipoPago.setSelectedIndex(factura.getTipoPago() - 1);

            float calcularPago = factura.calcularPago();
            float calcularIva = factura.calcularIva();

            this.txtCalcularPago.setText(String.valueOf(calcularPago));
            this.txtCalcularIva.setText(String.valueOf(calcularIva));
        } else {
            JOptionPane.showMessageDialog(this, "No hay datos de factura disponibles para mostrar.");
        }
       
    }//GEN-LAST:event_btmMostrarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JintFacturas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JintFacturas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JintFacturas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JintFacturas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                JintFacturas dialog = new JintFacturas(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btmCancelar;
    private javax.swing.JButton btmCerrar;
    private javax.swing.JButton btmGuardar;
    private javax.swing.JButton btmLimpiar;
    private javax.swing.JButton btmMostrar;
    private javax.swing.JButton btmNuevo;
    private javax.swing.JComboBox<String> cmbTipoPago;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField txtCalcularIva;
    private javax.swing.JTextField txtCalcularPago;
    private javax.swing.JTextField txtCantidad;
    private javax.swing.JTextField txtConcepto;
    private javax.swing.JTextField txtFecha;
    private javax.swing.JTextField txtNumNota;
    private javax.swing.JTextField txtPrecio;
    // End of variables declaration//GEN-END:variables
    private Facturas factura = new Facturas();
    //se termino el programa
}
