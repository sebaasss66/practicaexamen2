/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicaexamen2;

/**
 *
 * @author sebas
 */
abstract class Producto {
    protected int idProdcuto;
    protected String nombre;
    protected int unidadProducto;
    protected int precioUnitario;

    public Producto() {
        this.idProdcuto = 0;
        this.nombre = "";
        this.unidadProducto = 0;
        this.precioUnitario = 0;
    }

    public int getIdProdcuto() {
        return idProdcuto;
    }

    public void setIdProdcuto(int idProdcuto) {
        this.idProdcuto = idProdcuto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getUnidadProducto() {
        return unidadProducto;
    }

    public void setUnidadProducto(int unidadProducto) {
        this.unidadProducto = unidadProducto;
    }

    public int getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(int precioUnitario) {
        this.precioUnitario = precioUnitario;
    }
    
    public abstract float calcularPago();
    
    
    
    
}
