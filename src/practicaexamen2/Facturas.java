/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicaexamen2;

/**
 *
 * @author sebas
 */
public class Facturas extends NotaVenta implements Iva {
    private String rfc;
    private String nombre;
    private String domicilio;
    private String fechaFactura; 

    public Facturas() {
        super();
        this.rfc = "";
        this.nombre = "";
        this.domicilio = "";
        this.fechaFactura = "";
    }

    public Facturas(int numNota, String fecha, String concepto, Producto producto, int cantidad, int tipoPago, String rfc, String nombre, String domicilio, String fechaFactura) {
        super(numNota, fecha, concepto, producto, cantidad, tipoPago);
        this.rfc = rfc;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.fechaFactura = fechaFactura;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getFechaFactura() {
        return fechaFactura;
    }

    public void setFechaFactura(String fechaFactura) {
        this.fechaFactura = fechaFactura;
    }

    @Override
    public float calcularIva() {
        float subtotal = super.calcularPago(); // Calcular el subtotal
        return subtotal * 0.16f; // Calcular el IVA sobre el subtotal
    }

    @Override
    public float calcularPago() {
        float subtotal = super.calcularPago(); // Calcular el subtotal
        float iva = calcularIva(); // Calcular el IVA
        return subtotal + iva; // Sumar el subtotal y el IVA
    }

    void setProductoPerecedero(NoPerecederos producto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
