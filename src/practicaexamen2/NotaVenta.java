/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicaexamen2;

/**
 *
 * @author sebas
 */
public abstract class NotaVenta {
    protected int numNota;
    protected String fecha;
    protected String concepto;
    private Producto producto;
    protected int cantidad;
    protected int tipoPago;

    public NotaVenta(int numNota, String fecha, String concepto, Producto producto, int cantidad, int tipoPago) {
        this.numNota = numNota;
        this.fecha = fecha;
        this.concepto = concepto;
        this.producto = producto;
        this.cantidad = cantidad;
        this.tipoPago = tipoPago;
    }

    public NotaVenta() {
        this.numNota = 0;
        this.fecha = "";
        this.concepto = "";
        this.producto = null;
        this.cantidad = 0;
        this.tipoPago = 0;
    }

    public int getNumNota() {
        return numNota;
    }

    public void setNumNota(int numNota) {
        this.numNota = numNota;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getTipoPago() {
        return tipoPago;
    }

    public void setTipoPago(int tipoPago) {
        this.tipoPago = tipoPago;
    }

  public float calcularPago() {
        return producto.getPrecioUnitario() * cantidad;
    }
    
    
}
