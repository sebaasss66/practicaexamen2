/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicaexamen2;

/**
 *
 * @author sebas
 */
public class NoPerecederos extends Producto{
    
    private int loteFabricacion;

    public NoPerecederos() {
        this.loteFabricacion = 0;
    }

    public NoPerecederos(int loteFabricacion) {
        this.loteFabricacion = loteFabricacion;
    }

    public int getLoteFabricacion() {
        return loteFabricacion;
    }

    public void setLoteFabricacion(int loteFabricacion) {
        this.loteFabricacion = loteFabricacion;
    }
    
    public float calcularProducto(){
         return getPrecioUnitario() * 1.50f;
    }

    @Override
    public float calcularPago() {
        
       return calcularProducto();
    }
    
    
    
}
