/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicaexamen2;

/**
 *
 * @author sebas
 */
public class Perecederos extends Producto{
    private String fecha;
    private String temperatura;

    public Perecederos() {
        this.fecha = "";
        this.temperatura = "";
    }

    public Perecederos(String fecha, String temperatura) {
        this.fecha = fecha;
        this.temperatura = temperatura;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(String temperatura) {
        this.temperatura = temperatura;
    }
    
   
    @Override
    public float calcularPago() {
        float incremento = 0;
        switch (getUnidadProducto()) {
            case 1: // Kg
                incremento = 0.03f;
                break;
            case 2: // Litros
                incremento = 0.05f;
                break;
            case 3: // Pieza
                incremento = 0.04f;
                break;
            
        }
        
      
        float precioConIncremento = getPrecioUnitario() * (1 + incremento);
        
        float precioFinal = precioConIncremento * 1.50f;
        
        return precioFinal;
    }
        
       
        
    }
    
    
    

